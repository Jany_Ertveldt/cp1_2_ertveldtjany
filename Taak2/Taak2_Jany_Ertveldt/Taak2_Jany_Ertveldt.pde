/*Jany Ertveldt
 jany.ertveldt@student.ehb.be
 Multec 2016-2017
 */

int degrees = 0;
int degrees2 = 0;
int numberCirkels = 20;
int angleDiff = 360/numberCirkels;
int number = 145;
int [] xPositions = new int [number];

import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;
Minim minim;
AudioPlayer groove;

void setup() {
  //zorgt ervoor dat de rechthoeken ronddraaien in het midden van het scherm
  rectMode(CENTER);
  fullScreen();
  minim = new Minim(this);
  groove = minim.loadFile("song.mp3");
  groove.loop();
}

/*-----------------------------------------------------------
 -----------------------------------------------------------------------------
 -----------------------------------------------------------------------------
 -------------------------------------------------------*/



void draw() {
  background(0);
  if (key == '1') {
    hoizontaleEllipsen();
  }
  int beat = round(groove.left.level()*width)/2;
  //Centrale cirkel van allemaal rechthoeken dat blijft ronddraaien
  for ( int i = 0; i<numberCirkels; i++) {
    float degreesInRadiance = radians(degrees + i * angleDiff);
    int x =round(width/2 + cos(degreesInRadiance) * 200);
    int y =round(height/2 + sin(degreesInRadiance) * 200);

    noFill();
    stroke(255);
    rect(x, y, beat/2, beat/2);
  }
  if (degrees == 360) {
    degrees = 0;
  } else {
    degrees++;
  }
  degrees++;
  degrees = degrees % 360;
  println(degrees);
  noStroke();



  /*-----------------------------------------------------------
   -----------------------------------------------------------------------------
   -----------------------------------------------------------------------------
   -------------------------------------------------------*/


  //key-toetsen instellen als je er op drukt dat er een animatie begint
  if (key == '2') {
    witteLijn();
  } 
  if ( key =='3') {
    tweeRodeCirkels();
  } 
  if (key == '4') {
    middenRechthoeken();
  }
}

/*-----------------------------------------------------------
 -----------------------------------------------------------------------------
 -----------------------------------------------------------------------------
 -------------------------------------------------------*/
//zorgt ervoor dat op de x-as met y=0 en y=height ellipsen bewegen op de beat
void hoizontaleEllipsen() {
  fill(255);
  for (int i = 0; i < number; i++) {
    xPositions[i]= 0 + (i*10);
  }

  int beat = round(groove.left.level()*width*2);

  background(0);
  for (int i = 0; i < number; i++) {
    //yPositions[i]= round(random(0,400)); 

    ellipse( xPositions[i], 0, 3, 3+beat/random(2, 7));
    ellipse( xPositions[i], 0+height, 3, 3+beat/random(2, 7));
  }
}
/*-----------------------------------------------------------
 -----------------------------------------------------------------------------
 -----------------------------------------------------------------------------
 -------------------------------------------------------*/

//Een lijn van rode rechthoeken die op de beat bewegen

void witteLijn() {
  for (int j=0; j<2; j++) { 
    int numberCirkels = 100;
    int angleDiff = 360/numberCirkels;
    for ( int i = 0; i<numberCirkels; i++) {
      int beat = round(groove.left.level()*width)/2;
      float degreesInRadiance = radians(degrees + i * angleDiff);

      int y =round(height/2 + sin(degreesInRadiance) * 100);

      noFill();
      stroke(255);
      rect(0+(i*20), y, beat/random(2, 7), beat/random(2, 4));
      noStroke();
    }
  }
}
/*-----------------------------------------------------------
 -----------------------------------------------------------------------------
 -----------------------------------------------------------------------------
 -------------------------------------------------------*/

//Zorgt ervoor dat aan beide zijkanten van de centrale witte cirkel 
//een cirkel verschijnt  van rode rechthoeken
void tweeRodeCirkels() {
  int numberCirkels = 20;
  int angleDiff = 360/numberCirkels;
  int beat = round(groove.left.level()*width)/2;
  noFill();
  stroke(255, 0, 0);
  for ( int i = 0; i<numberCirkels; i++) {
    float degreesInRadiance = radians(degrees + i * angleDiff);
    int x =round(width/4 + cos(degreesInRadiance) * 50);
    int y =round(height/2 + sin(degreesInRadiance) * 50);
    rect(x-beat, y, beat/2, beat/2);

    for ( int j = 0; j<numberCirkels; j++) {
      float degreesInRadiance2 = radians(degrees + j * angleDiff);
      int x1 =round(width-width/4 + cos(degreesInRadiance2) * 50);
      int y1 =round(height/2 + sin(degreesInRadiance2) * 50);
      rect(x1+beat, y1, beat/2, beat/2);
    }
  }
  noStroke();
}

/*-----------------------------------------------------------
 -----------------------------------------------------------------------------
 -----------------------------------------------------------------------------
 -------------------------------------------------------*/

//Zorgt ervor dat in het midden van het scherm rechthoeken op de beat op en neer bewegen
void middenRechthoeken() {
  int beat= round(groove.left.level()*width)/2;
  fill(255-beat*5, 0+beat, 0);
  for (int d=0; d<120; d++) {
    frameRate(20);
    rect(0+(d*20), height/2, 15, 50+beat/random(1, 8));
  }
}